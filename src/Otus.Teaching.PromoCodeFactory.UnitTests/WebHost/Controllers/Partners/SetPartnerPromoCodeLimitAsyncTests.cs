﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 12,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    },
                     new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab147393"),
                        CreateDate = new DateTime(2020, 11, 5),
                        EndDate = new DateTime(2020, 12, 5),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
         public  SetPartnerPromoCodeLimitRequest CreateRequest()
        {
            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today,
                Limit = 150
            };

            return request;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
             var request = CreateRequest();    

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
 
            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NotActive_Returns400()
        {
           // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.IsActive = false;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = CreateRequest(); 

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
 
            // Assert
             result.Should().BeAssignableTo<BadRequestObjectResult>("Данный партнер не активен");
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessThanZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = CreateRequest();    
            request.Limit = -3;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_ReturnsNullNumberOfPromocodes()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
           
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = CreateRequest();  

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
            var result = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.NumberIssuedPromoCodes;
            // Assert
            result.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitCancelled_ReturnsNumberOfPromocodes()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
             List<PartnerPromoCodeLimit> limits = new List<PartnerPromoCodeLimit>();
            foreach(var par in partner.PartnerLimits)
                {
                par.CancelDate = DateTime.Now;
                limits.Add(par);
                }
            partner.PartnerLimits = limits;
            
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var request = CreateRequest();  

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
            var result = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.NumberIssuedPromoCodes;
            // Assert
            result.Should().BeGreaterThan(0);
        }

         [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_CancelActiveLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
           
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var limitBeforeSetup = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            var request = CreateRequest();  

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
             var limitAfterSetup = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.PartnerLimits.FirstOrDefault(x => 
                x.Id == limitBeforeSetup.Id);
            // Assert
            limitAfterSetup.CancelDate.Should().NotBeNull();
        }
         [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_LimitSaved()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
           
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
           
            var request = CreateRequest();  

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
              
            var newLimit = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.PartnerLimits.Single(x => 
                x.Limit == request.Limit && x.EndDate == request.EndDate);
            // Assert
            newLimit.Should().NotBeNull();
        }
    }
}