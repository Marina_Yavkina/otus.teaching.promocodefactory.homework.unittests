using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace StateMachine.BusinessLogic.Managers.UnitTests_Demo
{
    public class LotDocumentServiceTests_InMemory: IClassFixture<TestFixture_InMemory>
    {
    private readonly PartnersController _partnersController;
    private readonly DataContext _context;
     private readonly IRepository<Partner> _partnersRepository;

        public LotDocumentServiceTests_InMemory(TestFixture_InMemory testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
           _context = serviceProvider.GetService(typeof(DataContext)) as DataContext;
           _partnersRepository = serviceProvider.GetService(typeof(EfRepository<>)) as EfRepository<Partner>;
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 12,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    },
                     new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab147393"),
                        CreateDate = new DateTime(2020, 11, 5),
                        EndDate = new DateTime(2020, 12, 5),
                        Limit = 100
                    }
                }
            };

            return partner;
        }
        

        
        // [Fact]
        // public async Task Create_Should_Add_Limit_Successfully()
        // {
        //     var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
        //     Partner partner = CreateBasePartner();
           
        //     _partnersRepository.Setup(repo => repo.GetByIdAsync(partnerId))
        //         .ReturnsAsync(partner);
           
        //     var request = CreateRequest();  

        //     // Act
        //     await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId,request);
              
        //     var newLimit = _partnersRepositoryMock.Object.GetByIdAsync(partnerId).Result.PartnerLimits.Single(x => 
        //         x.Limit == request.Limit && x.EndDate == request.EndDate);
        //     // Assert
        //     newLimit.Should().NotBeNull();
        //     //Arrange
        //     var lotId = Guid.NewGuid();
        //     var fileId = Guid.NewGuid();
        //     var type = LotDocumentType.Protocol1;
        //     var name = "1";

        //     _lotRepository.Add(new Lot
        //     {
        //         Id = lotId
        //     });
        //     await _lotRepository.SaveChangesAsync();

        //     //Act
        //     var result = await _lotDocumentService.CreateAsync(lotId, fileId, type, name);
            
        //     //Assert
        //     Assert.NotEqual(Guid.Empty, result);
        // }
    }
}